var gulp = require("gulp"),
  uglify = require("gulp-uglify"),
  rename = require("gulp-rename"),
  sass = require("gulp-sass"),
  sourcemaps = require("gulp-sourcemaps");
(prefix = require("gulp-autoprefixer")),
  (livereload = require("gulp-livereload")),
  (concat = require("gulp-concat")),
  (browserSync = require("browser-sync").create());

let autoprefixBrowsers = [
  "> 1%",
  "last 2 versions",
  "firefox >= 4",
  "safari 7",
  "safari 8",
  "IE 9",
  "IE 10",
  "IE 11",
];

gulp.task("css", function () {
  gulp
    .src("source/resources/assets/sass/*.scss")
    .pipe(sourcemaps.init())
    .pipe(
      sass({
        outputStyle: "compressed",
      }).on("error", sass.logError)
    )
    .pipe(
      prefix({
        browsers: autoprefixBrowsers,
      })
    )
    .pipe(rename({ suffix: ".min" }))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest("public/css"))
    .pipe(livereload())
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task("js", function () {
  return gulp
    .src("source/resources/assets/js/dev/*.js")
    .pipe(concat("main.min.js"))
    .pipe(uglify())
    .on("error", function (e) {
      console.log(e.message);
      this.emit("end");
    })
    .pipe(gulp.dest("public/js"))
    .pipe(livereload())
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task("js-vendor", function () {
  return gulp
    .src("source/resources/assets/js/vendor/*.js")
    .pipe(concat("vendor.min.js"))
    .pipe(uglify())
    .on("error", function (e) {
      console.log(e.message);
      this.emit("end");
    })
    .pipe(gulp.dest("public/js"))
    .pipe(livereload());
});

gulp.task("copy-php", function () {
  gulp
    .src(["source/app/*.php", "source/app/*/*.php"])
    .pipe(gulp.dest("public"))
    .pipe(livereload())
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task("copy-fonts", function () {
  gulp
    .src(["source/resources/assets/fonts/*"])
    .pipe(gulp.dest("public/assets/fonts"))
    .pipe(livereload())
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task("copy-images", function () {
  gulp
    .src(["source/resources/assets/images/*"])
    .pipe(gulp.dest("public/assets/images"))
    .pipe(livereload())
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task("serve", ["css"], function () {
  browserSync.init({
    open: "external",
    host: "blacklinecreative.local",
    proxy: "http://blacklinecreative.local",
  });
});

gulp.task("watch", function () {
  livereload.listen();
  gulp.watch("source/resources/assets/sass/**/*.scss", ["css"]);
  gulp
    .watch("source/resources/assets/js/dev/*.js", ["js"])
    .on("change", browserSync.reload);
  gulp
    .watch("source/resources/assets/js/vendor/*.js", ["js-vendor"])
    .on("change", browserSync.reload);
  gulp
    .watch("source/app/*/*.php", ["copy-php"])
    .on("change", browserSync.reload);
  gulp.watch("source/app/*.php", ["copy-php"]).on("change", browserSync.reload);
  //gulp.watch('source/app/*/*.php', ['vers']).on('change', browserSync.reload);
  //gulp.watch('source/app/*.php', ['vers']).on('change', browserSync.reload);
});

//gulp.task('default', ['css', 'js', 'js-vendor','vers', 'copy-php', 'copy-images', 'copy-fonts','watch', 'serve']);
gulp.task("default", [
  "css",
  "js",
  "js-vendor",
  "copy-php",
  "copy-images",
  "copy-fonts",
  "watch",
  "serve",
]);
gulp.task("build", [
  "css",
  "js",
  "js-vendor",
  "copy-php",
  "copy-images",
  "copy-fonts",
]);
