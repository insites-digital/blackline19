<?php $config = require_once '../inc/config.php'; ?>
<?php include  '../inc/header.php'; ?>

<?php // include  'components/component_article.php'; ?>

<main>
	<article id="article-content">
	  <div class="credits component component_text-block">
	    <div class="content">
	        <p>We are an independent creative agency that enjoys making things look, feel, sound and work better than people imagine they can.</p>
	        <p>Brand / Strategy / Campaign / Experience / Performance</p>
	    </div>
		</div>  
		<div class="contact component component_text-block">
	    <h2 class="brand">Contact</h2>
	    <div class="content">
	      <p><a href="mailto:hello@blacklinecreative.co.uk">hello@blacklinecreative.co.uk</a>
	      <br>
	      <a tel="+442030112450">+44(0)20 3011 2450</a>
	      </p>    
	      <p>The Old Truman Brewery<br>
	          91 Brick Lane<br>
	          London E1 6QL</p>
	      <div class="social-links">
					<a href="https://www.instagram.com/blackline_creative/" target="_blank">Instagram</a>
					<a href="https://www.linkedin.com/company/blackline-creative" target="_blank">LinkedIn</a>
	      </div>
	    </div>
		</div>  
	</article>
</main>


<?php include  '../inc/footer.php'; ?>
