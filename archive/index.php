<?php $config = require_once 'inc/config.php'; ?>
<?php include  'inc/header.php'; ?>

<main>
	<div class="parallax">
		<div class="parallax__group section-one">
			<div class="parallax__layer section-title">
				<svg width="1586px" height="197px" viewBox="0 0 1586 197" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
		        <g id="BLACK001_Website_Template_V6_just-design" transform="translate(-529.000000, -1102.000000)" fill="#000000" fill-rule="nonzero">
	            <g id="Section-Titles" transform="translate(483.241100, 1091.593775)">
                <path d="M46.719725,207 L46.719725,10.9453125 L92.2470688,10.9453125 L92.2470688,207 L46.719725,207 Z M349.688475,108.972656 C349.688475,164.207031 317.833006,207 251.79785,207 L129.161131,207 L129.161131,10.9453125 L251.79785,10.9453125 C317.833006,10.9453125 349.688475,53.875 349.688475,108.972656 Z M174.688475,50.4570312 L174.688475,167.488281 L251.79785,167.488281 C289.532225,167.488281 302.793944,139.324219 302.793944,108.972656 C302.793944,78.6210938 289.532225,50.4570312 251.79785,50.4570312 L174.688475,50.4570312 Z M425.293944,88.875 L564.747069,88.875 L564.747069,127.019531 L425.293944,127.019531 L425.293944,167.488281 L571.993163,167.488281 L571.993163,207 L379.7666,207 L379.7666,10.9453125 L570.625975,10.9453125 L570.625975,50.4570312 L425.293944,50.4570312 L425.293944,88.875 Z M604.805663,207 L604.805663,10.9453125 L662.774413,10.9453125 L771.602538,153.40625 L771.602538,10.9453125 L817.129881,10.9453125 L817.129881,207 L759.161131,207 L650.333006,64.5390625 L650.333006,207 L604.805663,207 Z M844.473631,10.9453125 L1058.43848,10.9453125 L1058.43848,50.4570312 L974.219725,50.4570312 L974.219725,207 L928.692381,207 L928.692381,50.4570312 L844.473631,50.4570312 L844.473631,10.9453125 Z M1087.14941,207 L1087.14941,10.9453125 L1132.67676,10.9453125 L1132.67676,207 L1087.14941,207 Z M1161.38769,10.9453125 L1375.35254,10.9453125 L1375.35254,50.4570312 L1291.13379,50.4570312 L1291.13379,207 L1245.60644,207 L1245.60644,50.4570312 L1161.38769,50.4570312 L1161.38769,10.9453125 Z M1631.56348,10.9453125 L1533.80957,128.25 L1533.80957,207 L1488.14551,207 L1488.14551,128.25 L1390.3916,10.9453125 L1443.16504,10.9453125 L1510.97754,92.703125 L1578.79004,10.9453125 L1631.56348,10.9453125 Z" id="IDENTITY"></path>
	            </g>
		        </g>
			    </g>
				</svg>
			</div>
			<div class="parallax__layer parallaxCenterTop">
				<video muted playsinline autoplay loop class="lazy" rel="showVideo">
					<data-src src="https://player.vimeo.com/external/325631622.hd.mp4?s=23a0540dde30aae0bd55fefaabf1c6d2e97908dd&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>  
			<div class="parallax__layer blob blob-first">
				<img class="lazy" data-src="assets/images/BL_GREEN_2-min.png">
			</div>   
			<div class="parallax__layer parallaxLeftTop">
				<img class="lazy" data-src="assets/images/Letters.gif">
			</div>   
			<div class="parallax__layer parallaxLeftDeep">
				<img class="lazy" data-src="assets/images/Men_CMYK-min.jpg">
			</div>   
			<div class="parallax__layer parallaxRight">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/325631950.hd.mp4?s=eaa4e4a2e097c01250d93df5786196241e2a7562&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>   
			<div class="parallax__layer parallaxDeep">
				<img class="lazy" data-src="assets/images/Roof-Pattern-min.jpg">
			</div>   
		</div>

		<div class="parallax__group section-two">
			<div class="parallax__layer section-title">
				<svg width="1572px" height="215px" viewBox="0 0 1572 215" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
		        <g id="BLACK001_Website_Template_V6_just-design" transform="translate(-495.000000, -3800.000000)" fill="#000000" fill-rule="nonzero">
	            <g id="Section-Titles" transform="translate(483.241100, 1091.593775)">
                <path d="M61.3134766,2816.13574 C61.3134766,2850.68652 85.6689453,2882.40527 148.115234,2882.40527 C187.055664,2882.40527 216.367188,2865.83788 216.367188,2865.83788 L238.881836,2898.83105 C238.881836,2898.83105 211.694336,2923.32812 148.115234,2923.32812 C56.0742188,2923.32812 12.7441406,2874.33398 12.7441406,2816.13574 C12.7441406,2757.93749 56.0742188,2708.94335 148.115234,2708.94335 C211.694336,2708.94335 238.881836,2733.44042 238.881836,2733.44042 L216.367188,2766.43359 C216.367188,2766.43359 187.055664,2749.8662 148.115234,2749.8662 C85.6689453,2749.8662 61.3134766,2781.58495 61.3134766,2816.13574 Z M375.952148,2923.32812 C295.522461,2923.32812 267.202148,2884.9541 267.202148,2825.48144 L267.202148,2714.60742 L314.355469,2714.60742 L314.355469,2825.48144 C314.355469,2862.01464 327.524414,2882.40527 375.952148,2882.40527 C424.379883,2882.40527 437.548828,2862.01464 437.548828,2825.48144 L437.548828,2714.60742 L484.702148,2714.60742 L484.702148,2825.48144 C484.702148,2884.9541 456.381836,2923.32812 375.952148,2923.32812 Z M568.671875,2714.60742 L568.671875,2876.7412 L709.990234,2876.7412 L709.990234,2917.66406 L521.518555,2917.66406 L521.518555,2714.60742 L568.671875,2714.60742 Z M633.321937,2714.60742 L854.928383,2714.60742 L854.928383,2755.53027 L767.70182,2755.53027 L767.70182,2917.66406 L720.5485,2917.66406 L720.5485,2755.53027 L633.321937,2755.53027 L633.321937,2714.60742 Z M991.998695,2923.32812 C911.569008,2923.32812 883.248695,2884.9541 883.248695,2825.48144 L883.248695,2714.60742 L930.402016,2714.60742 L930.402016,2825.48144 C930.402016,2862.01464 943.570961,2882.40527 991.998695,2882.40527 C1040.42643,2882.40527 1053.59537,2862.01464 1053.59537,2825.48144 L1053.59537,2714.60742 L1100.7487,2714.60742 L1100.7487,2825.48144 C1100.7487,2884.9541 1072.42838,2923.32812 991.998695,2923.32812 Z M1184.71842,2917.66406 L1137.5651,2917.66406 L1137.5651,2714.60742 L1270.95377,2714.60742 C1326.03678,2714.60742 1350.81705,2747.45898 1350.81705,2783.28417 C1350.81705,2813.58691 1335.38248,2832.70312 1308.61979,2841.62402 L1352.79948,2917.66406 L1300.2653,2917.66406 L1259.62565,2847.71288 L1184.71842,2847.71288 L1184.71842,2917.66406 Z M1270.95377,2755.53027 L1184.71842,2755.53027 L1184.71842,2811.03808 L1270.95377,2811.03808 C1295.45084,2811.03808 1303.09733,2798.01074 1303.09733,2783.28417 C1303.09733,2768.55761 1295.45084,2755.53027 1270.95377,2755.53027 Z M1431.10514,2795.32031 L1575.53873,2795.32031 L1575.53873,2834.82714 L1431.10514,2834.82714 L1431.10514,2876.7412 L1583.04362,2876.7412 L1583.04362,2917.66406 L1383.95182,2917.66406 L1383.95182,2714.60742 L1581.6276,2714.60742 L1581.6276,2755.53027 L1431.10514,2755.53027 L1431.10514,2795.32031 Z" id="CULTURE"></path>
	            </g>
		        </g>
			    </g>
				</svg>
			</div>
			<div class="parallax__layer parallaxCenterTop">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/325632016.hd.mp4?s=9d89b58a1d10b7c6c2614cf4c4b22702534ee23c&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>  
			<div class="parallax__layer parallaxLeftMid">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/325632059.hd.mp4?s=a042e2885817dc72384432dbf28dcc03cab5fe1c&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>   
			<div class="parallax__layer parallaxRight">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/328458329.hd.mp4?s=084db9d3ef649413d5dff872fe8cbd1d3ebbfce4&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>   
			<div class="parallax__layer parallaxDeep">
				<video muted playsinline autoplay loop class="lazy" rel="showVideo">>
					<data-src src="https://player.vimeo.com/external/325632120.hd.mp4?s=47aa0a2113a40cfdb05ae54ad1339cf83d556d19&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>   
		</div>

		<div class="parallax__group section-three">
			<div class="parallax__layer parallaxLeftMid">
				<img class="lazy" data-src="assets/images/Tech-image-min.jpg">
			</div>   
			<div class="parallax__layer parallaxLeftTop first">
				<img class="lazy" data-src="assets/images/PopArtPoster-min.jpg">
			</div>   
			<div class="parallax__layer parallaxCenterTop first">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/325632483.hd.mp4?s=8855e651f288062beafef0f3e4c8011e41c19376&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>  
			<div class="parallax__layer parallaxRight first">
				<img class="lazy" data-src="assets/images/Shoes_2-min.jpg">
			</div>   
			<div class="parallax__layer parallaxDeep">
				<img class="lazy" data-src="assets/images/Crown-min.jpg">
			</div>   
			<div class="parallax__layer parallaxCenterTop second">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/325632543.hd.mp4?s=5abde5d29b3848fe1b1eb46ccacac88471abcb8c&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>  
			<div class="parallax__layer parallaxLeftTop second">
				<img class="lazy" data-src="assets/images/Construction-icons.gif">
			</div>   
			<div class="parallax__layer parallaxRight second">
				<img class="lazy" data-src="assets/images/BYO-min.jpg">
			</div>   
			<div class="parallax__layer parallaxDeep second">
				<img class="lazy" data-src="assets/images/Beats-min.jpg">
			</div>   
		</div>

		<div class="parallax__group section-four">
			<div class="parallax__layer section-title">
				<svg width="1431px" height="253px" viewBox="0 0 1431 253" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
		        <g id="BLACK001_Website_Template_V6_just-design" transform="translate(-591.000000, -8290.000000)" fill="#000000" fill-rule="nonzero">
	            <g id="Section-Titles" transform="translate(495.241100, 1101.593775)">
                <path d="M153.708991,7315.1675 C153.708991,7355.79446 182.347663,7393.09133 255.775886,7393.09133 C301.56446,7393.09133 336.030769,7373.61037 336.030769,7373.61037 L362.50489,7412.40578 C362.50489,7412.40578 330.53614,7441.21096 255.775886,7441.21096 C147.548347,7441.21096 96.5981516,7383.60061 96.5981516,7315.1675 C96.5981516,7246.7344 147.548347,7189.12405 255.775886,7189.12405 C330.53614,7189.12405 362.50489,7217.92922 362.50489,7217.92922 L336.030769,7256.72463 C336.030769,7256.72463 301.56446,7237.24367 255.775886,7237.24367 C182.347663,7237.24367 153.708991,7274.54055 153.708991,7315.1675 Z M451.251472,7434.5508 L395.805671,7434.5508 L395.805671,7195.7842 L552.652351,7195.7842 C617.42237,7195.7842 646.560554,7234.41311 646.560554,7276.5386 C646.560554,7312.17043 628.411628,7334.64846 596.94239,7345.13821 L648.891609,7434.5508 L587.118659,7434.5508 L539.332038,7352.29787 L451.251472,7352.29787 L451.251472,7434.5508 Z M552.652351,7243.90383 L451.251472,7243.90383 L451.251472,7309.17336 L552.652351,7309.17336 C581.457527,7309.17336 590.448738,7293.855 590.448738,7276.5386 C590.448738,7259.22219 581.457527,7243.90383 552.652351,7243.90383 Z M915.79737,7434.5508 L890.488777,7387.76321 L754.954597,7387.76321 L729.646003,7434.5508 L670.537116,7434.5508 L799.910652,7195.7842 L845.532722,7195.7842 L974.906257,7434.5508 L915.79737,7434.5508 Z M822.721687,7262.38576 L780.096687,7341.30862 L865.346687,7341.30862 L822.721687,7262.38576 Z M1058.65772,7294.02151 L1228.49171,7294.02151 L1228.49171,7340.4761 L1058.65772,7340.4761 L1058.65772,7434.5508 L1003.21192,7434.5508 L1003.21192,7195.7842 L1235.65137,7195.7842 L1235.65137,7243.90383 L1058.65772,7243.90383 L1058.65772,7294.02151 Z M1265.62208,7195.7842 L1526.20069,7195.7842 L1526.20069,7243.90383 L1423.63428,7243.90383 L1423.63428,7434.5508 L1368.18848,7434.5508 L1368.18848,7243.90383 L1265.62208,7243.90383 L1265.62208,7195.7842 Z" id="CRAFT"></path>
	            </g>
		        </g>
			    </g>
				</svg>
			</div>
			<div class="parallax__layer blob blob-first">
				<img class="lazy" data-src="assets/images/BL_GREEN_2-min.png">
			</div>   
			<div class="parallax__layer parallaxLeftTop first">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/325632619.hd.mp4?s=91da3ac87dfe50d8db64fcc8267f1d71849c2b3e&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>   
			<div class="parallax__layer parallaxCenterTop first">
				<img class="lazy" data-src="assets/images/Icons.gif">
			</div>  
			<div class="parallax__layer parallaxDeep">
				<img class="lazy" data-src="assets/images/Home-min.jpg">
			</div>   
			<div class="parallax__layer parallaxLeftTop second">
				<img class="lazy" data-src="assets/images/kingston.svg">
			</div>   
			<div class="parallax__layer parallaxDeep second">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/325632712.hd.mp4?s=a5409803f7b3f3d242ea058fd739258958bfa051&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>   
		</div>
		
		<div class="parallax__group section-five">
			<div class="parallax__layer section-title">
				
				<svg width="1630px" height="253px" viewBox="0 0 1630 253" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
		        <g id="BLACK001_Website_Template_V6_just-design" transform="translate(-496.000000, -1542.000000)" fill="#000000" fill-rule="nonzero">
	            <path d="M651.406848,1597.47901 L551.671009,1597.47901 L551.671009,1668.40967 L651.406848,1668.40967 C678.213977,1668.40967 691.201282,1655.25586 691.201282,1632.94434 C691.201282,1610.63281 678.213977,1597.47901 651.406848,1597.47901 Z M551.671009,1788.12598 L496.225208,1788.12598 L496.225208,1549.35938 L651.406848,1549.35938 C710.349231,1549.35938 747.313098,1579.66309 747.313098,1632.94434 C747.313098,1686.22559 710.349231,1716.5293 651.406848,1716.5293 L551.671009,1716.5293 L551.671009,1788.12598 Z M842.719837,1549.35938 L842.719837,1740.00635 L1008.89074,1740.00635 L1008.89074,1788.12598 L787.274036,1788.12598 L787.274036,1549.35938 L842.719837,1549.35938 Z M1275.7965,1788.12598 L1250.4879,1741.33838 L1114.95372,1741.33838 L1089.64513,1788.12598 L1030.53624,1788.12598 L1159.90978,1549.35938 L1205.53185,1549.35938 L1334.90538,1788.12598 L1275.7965,1788.12598 Z M1182.72081,1615.96094 L1140.09581,1694.88379 L1225.34581,1694.88379 L1182.72081,1615.96094 Z M1373.56626,1668.74268 C1373.56626,1709.36963 1402.20493,1746.66651 1475.63315,1746.66651 C1521.42173,1746.66651 1555.88804,1727.18555 1555.88804,1727.18555 L1582.36216,1765.98096 C1582.36216,1765.98096 1550.39341,1794.78613 1475.63315,1794.78613 C1367.40561,1794.78613 1316.45542,1737.17578 1316.45542,1668.74268 C1316.45542,1600.30957 1367.40561,1542.69922 1475.63315,1542.69922 C1550.39341,1542.69922 1582.36216,1571.5044 1582.36216,1571.5044 L1555.88804,1610.29981 C1555.88804,1610.29981 1521.42173,1590.81885 1475.63315,1590.81885 C1402.20493,1590.81885 1373.56626,1628.11572 1373.56626,1668.74268 Z M1671.10874,1644.2666 L1840.94272,1644.2666 L1840.94272,1690.72119 L1671.10874,1690.72119 L1671.10874,1740.00635 L1849.76743,1740.00635 L1849.76743,1788.12598 L1615.66294,1788.12598 L1615.66294,1549.35938 L1848.10239,1549.35938 L1848.10239,1597.47901 L1671.10874,1597.47901 L1671.10874,1644.2666 Z M2118.50474,1566.84229 L2091.3646,1606.13721 C2091.3646,1606.13721 2058.23032,1590.81885 2003.61704,1590.81885 C1962.65708,1590.81885 1945.50718,1599.47705 1945.50718,1613.96289 C1945.50718,1630.11377 1966.48667,1635.10889 2011.60923,1643.9336 C2089.03354,1659.08545 2125.83091,1675.40283 2125.83091,1721.85742 C2125.83091,1769.97705 2080.54184,1794.78613 2003.61704,1794.78613 C1919.19956,1794.78613 1879.73813,1766.48047 1879.73813,1766.48047 L1906.87827,1727.18555 C1906.87827,1727.18555 1945.00766,1746.66651 2003.61704,1746.66651 C2048.7396,1746.66651 2069.71909,1737.50879 2069.71909,1721.85742 C2069.71909,1705.87305 2053.40171,1703.20899 2001.11948,1692.55274 C1917.03501,1675.40283 1889.39536,1654.42334 1889.39536,1613.96289 C1889.39536,1577.33203 1916.702,1542.69922 2003.61704,1542.69922 C2086.20298,1542.69922 2118.50474,1566.84229 2118.50474,1566.84229 Z" id="PLACES"></path>
		        </g>
			    </g>
				</svg>
			</div>
			<div class="parallax__layer parallaxLeftTop first">
				<img class="lazy" data-src="assets/images/All-Animals_onred.gif">
			</div>   
			<div class="parallax__layer parallaxDeep">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/325632855.hd.mp4?s=cfea806b4c0165107c26ef2f50fafff0412b0077&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>   
			<div class="parallax__layer parallaxRight first">
				<img class="lazy" data-src="assets/images/111.svg">
			</div>
			<div class="parallax__layer parallaxDeep second">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/326769389.hd.mp4?s=c720d502a1a004b93f36cb36239fa3fc0dd5134d&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>   
			<div class="parallax__layer parallaxRight second">
				<img class="lazy" data-src="assets/images/33-min.jpg">
			</div>
			<div class="parallax__layer parallaxCenterTop">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/325633001.hd.mp4?s=389aee6edd25ebf183eeef4b5604a9121ace42ca&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>
			<div class="parallax__layer parallaxLeftMid">
				<img class="lazy" data-src="assets/images/Knifes-and-Forks-min.jpg">
			</div>
			<div class="parallax__layer parallaxRight third">
				<img class="lazy" data-src="assets/images/Poster-Design-min.jpg">
			</div>
			<div class="parallax__layer parallaxDeep third">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/325633067.hd.mp4?s=bad89db0ae7f542fada627c564ff26cfbab28949&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>   
		</div>


		<div class="credits component component_text-block first">
			<div class="text-translate">
				<h2>Featured Clients</h2>
				<ul>
					<li>AVIVA</li>
					<li>CITY OF LONDON</li>
					<li>MEYER BERGMAN</li>
					<li>CITY OF WOLVERHAMPTON</li>
					<li>ROYAL BOROUGH OF KINGSTON</li>
					<li>PROMENADEN FASHION DISTRICT</li>
					<li>STEEN&STRØM</li>
					<li>EGER GALLERY</li>
					<li>BOROUGH YARDS</li>
					<li>LEADENHALL MARKET</li>
				</ul>
			</div>
		</div>	
		<div class="contact component component_text-block" style="padding-bottom: 300px;">
			<div class="text-translate">
				<h2>Contact</h2>
				<div class="content">
					<p><a href="mailto:hello@blacklinecreative.co.uk">hello@blacklinecreative.co.uk</a>
					<br>
					<a tel="+442030112450">+44(0)20 3011 2450</a>
					</p>	
					<p>The Old Truman Brewery<br>
						91 Brick Lane<br>
						London E1 6QL</p>
					<div class="social-links">
						<a href="https://www.instagram.com/blackline_creative/" target="_blank">Instagram</a>
						<a href="https://www.linkedin.com/company/blackline-creative" target="_blank">LinkedIn</a>
					</div>
				</div>
			</div>
		</div>	
		
	</div>
</main>


<?php include  'inc/footer.php'; ?>
