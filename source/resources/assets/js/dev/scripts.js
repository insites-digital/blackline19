function homepageParallax() {
  setTimeout(function() {
    jQuery(".parallax").addClass("in");
    jQuery(".parallax__group").addClass("fadeIn");
  }, 1000);
}

function addScrollHeight() {
  var height = 0;
  var layer = jQuery(".parallax__group");
  jQuery(layer).each(function() {
    $(this).css("height", "auto");
    height = height + $(this)[0].scrollHeight;
    $(this).height(height);
    height = 0;
  });
}

function addTextHeight() {
  var height = 0;
  var layer = jQuery(".component_text-block");
  jQuery(layer).each(function() {
    $(this).css("height", "auto");
    height = height + $(this)[0].scrollHeight;
    $(this).height(height);
    height = 0;
  });
}

function loadVisibleImages() {
  var layer = jQuery(".parallax__group");
  if ($(window).width() < 740) {
    var instance = $(".lazy").lazy({
      chainable: false
    });
    instance.loadAll();
  }
  jQuery(layer).each(function() {
    if ($(window).width() < 740) {
      if (!jQuery(this).attr("loaded")) {
        //not in ajax.success due to multiple sroll events
        jQuery(this).attr("loaded", true);
      }
    } else {
      if (
        jQuery(".parallax").scrollTop() + jQuery(window).height() >=
        jQuery(this).position().top
      ) {
        if (!jQuery(this).attr("loaded")) {
          //not in ajax.success due to multiple sroll events
          jQuery(this).attr("loaded", true);
        }
      }
    }
    randomLoad();
  });
}

function textFade() {
  var layer = jQuery(".component_text-block li");

  jQuery(layer).each(function() {
    if ($(window).width() > 740) {
      if (
        jQuery(".parallax").scrollTop() + jQuery(window).height() >=
        jQuery(this).position().top
      ) {
        if (!jQuery(this).attr("loaded")) {
          //not in ajax.success due to multiple sroll events
          jQuery(this).attr("loaded", true);
        }
      }
    } else {
      if (!jQuery(this).attr("loaded")) {
        //not in ajax.success due to multiple sroll events
        jQuery(this).attr("loaded", true);
      }
    }
  });
}

function randomLoad() {
  var $divs = $(".parallax__layer");
  var $ds = $divs.not(".load");

  if ($(window).width() < 740) {
    $ds.addClass("load");
  } else {
    var interval = setInterval(function() {
      $ds.eq(Math.floor(Math.random() * $ds.length)).addClass("load");

      if ($ds.length == 1) {
        clearInterval(interval);
      }
    }, 500);
  }
}

function loadPage(newUrl) {
  var httpRequest = new XMLHttpRequest();
  httpRequest.onreadystatechange = function() {
    if (httpRequest.readyState !== XMLHttpRequest.DONE) return;

    var newDocument = httpRequest.responseXML;
    if (newDocument === null) return;

    var newContent = httpRequest.responseXML.getElementById("article-content");

    document.title = newDocument.title;

    var contentElement = document.getElementById("article-content");
    contentElement.replaceWith(newContent);
    newContent.classList.add("loaded");
  };

  httpRequest.responseType = "document";
  httpRequest.open("GET", newUrl);
  httpRequest.send();
}

function loadLinks() {
  // Make links load asynchronously
  document.body.addEventListener("click", function(event) {
    var parentClass = event.target.parentNode.classList;

    if (event.target.tagName !== "A") return;

    // History API needed to make sure back and forward still work
    if (history === null) return;
    event.preventDefault();

    // External links should instead open in a new tab
    var newUrl = event.target.href;
    var domain = window.location.origin;
    if (typeof domain !== "string" || newUrl.search(domain) !== 0) {
      window.open(newUrl, "_blank");
    } else {
      if (!parentClass.contains("open")) {
        loadPage(newUrl);
      }
      history.pushState(null /*stateObj*/, "" /*title*/, newUrl);
    }
  });
}

function showlinks() {
  $(".borders div a").css("visibility", "visible");
}

function ajaxLinks() {
  var newHash = "",
    linkClass = "",
    $mainContent = $("#article-content");
  $(".borders div").delegate("a", "click touchstart", function() {
    //if(!$('.borders div').hasClass('open')) {
    window.location.hash = $(this).attr("href");
    linkClass = $(this).attr("id");
    //}
    return false;
  });

  $(window).bind("hashchange", function() {
    $("#article-content").removeClass("transition loaded");
    $("#article-content").empty();
    newHash = window.location.hash.substr(1);
    $mainContent.load(newHash + " #article-content > *");
    $mainContent.addClass("loaded");

    parentClass = $("#" + linkClass)
      .parent()
      .attr("class");

    if (parentClass.indexOf("left") >= 0) {
      parentClass = "left";
    } else {
      parentClass = "right";
    }

    $(".page-overlay").removeClass(
      "reveal-right-undo reveal-both reveal-right reveal-left reveal-left-undo"
    );
    if ($(".borders div").hasClass("open")) {
      $(".left, .right").toggleClass("open closed");
      if (!$("." + parentClass).hasClass("closed")) {
        $("." + parentClass).addClass("open");
      }
      $(".page-overlay")
        .removeClass("reveal-left reveal-right")
        .addClass("reveal-both");
      $(".close").toggleClass("left-close right-close");
      if (linkClass === "about") {
        history.pushState(null, "", "/contact");
      } else {
        history.pushState(null, "", "/about");
      }
      return;
    }

    if ($("." + parentClass).hasClass("open")) {
      $("#article-content")
        .addClass("transition")
        .removeClass("loaded");
      setTimeout(function() {
        $("#article-content")
          .promise()
          .done(function() {
            $("." + parentClass).toggleClass("open closed");
            $(".page-overlay").addClass("reveal-" + parentClass + "-undo");
            $(".close").removeClass(parentClass + "-close");
            history.pushState(null, "", "/");
          });
      }, 600);
    } else {
      $("." + parentClass)
        .removeClass("closed")
        .addClass("open");
      $("#article-content").addClass("loaded");
      $(".close").addClass(parentClass + "-close");
    }
    $(".page-overlay").toggleClass("reveal reveal-" + parentClass);
  });
}

function closeOverlay() {
  $(".close").delegate("svg", "click touchstart", function() {
    className = $(this)
      .parent()
      .attr("class");
    if (className.indexOf("left") >= 0) {
      className = "left";
    } else {
      className = "right";
    }
    if (!$(".page-overlay").hasClass("mobile-reveal")) {
      $(".page-overlay").removeClass(
        "reveal-" +
          className +
          "-undo reveal-both reveal-" +
          className +
          " reveal-" +
          className +
          " reveal-" +
          className +
          "-undo reveal"
      );
      $(".page-overlay").addClass("reveal-" + className + "-undo");
      $(".close").removeClass(className + "-close");
      $("#article-content")
        .addClass("transition")
        .removeClass("loaded");
      $("." + className).toggleClass("open closed");
      history.pushState(null, "", "/");
      $("#article-content").empty();
    } else {
      $(".page-overlay").removeClass("mobile-reveal");
      history.pushState(null, "", "/");
      $("#article-content").empty();
    }
  });
}

function mobileMenu() {
  $(".hamburger").on("click", function() {
    if ($("nav").hasClass("open")) {
      $("nav").removeClass("open");
      $("#article-content").empty();
    } else {
      $("nav").addClass("open");
    }
  });
  $("#main-nav a").on("click touchstart", function(e) {
    e.preventDefault();
    $("nav").removeClass("open");

    $(".page-overlay").toggleClass("mobile-reveal");

    window.location.hash = $(this).attr("href");

    $(window).bind("hashchange", function() {
      newHash = window.location.hash.substr(1);
      $("#article-content").load(newHash + " #article-content > *");
    });
  });
}

jQuery(document).ready(function($) {
  setTimeout(showlinks, 1000);
  homepageParallax();
  loadVisibleImages();
  if ($(window).width() < 740) {
    textFade();
  }
  ajaxLinks();
  closeOverlay();
  mobileMenu();
  addScrollHeight();
  addTextHeight();

  if ($(window).width() > 740) {
    $(".lazy").lazy({
      effect: "fadeIn",
      effectTime: 350,
      appendScroll: $(".parallax")
    });
    //Add Height when div on screen
    setInterval(function() {
      $(".parallax__group")
        .filter(":onScreen") // get only screen
        .height($(this)[0].scrollHeight); // give them a red background
    }, 1000); // repeat every second
  } else {
    $(".lazy").lazy({
      effect: "fadeIn",
      effectTime: 50,
      appendScroll: $(".parallax")
    });
  }
});

$(window).bind("resize", function(e) {
  console.log("window resized..");

  homepageParallax();
  loadVisibleImages();
  addScrollHeight();
  addTextHeight();
});

jQuery(".parallax").scroll(function() {
  if ($(window).width() > 740) {
    loadVisibleImages();
    textFade();
  }
});
