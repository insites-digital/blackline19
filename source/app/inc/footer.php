			<div class="borders">
				<div class="top" id="top"></div>
				<div class="bottom"></div>
				<div class="left closed">
					<a id="about" class="menu-link" href="/about">About</a>
				</div>
				<div class="right closed">
					<a id="contact" class="menu-link" href="/contact">Contact</a>
				</div>
			</div>
			
			<div id="overlay" class="page-overlay page-overlay_slider page-overlay_slider-left">
				<div class="close">
					<svg id="close" width="73px" height="75px" viewBox="0 0 73 75" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
				    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round">
			        <g id="BLACK001_Website_Template_V6_just-design" transform="translate(-1242.000000, -207.000000)" stroke="#00ff95" stroke-width="4">
		            <g id="Close-Icon" transform="translate(1242.708892, 208.208892)">
	                <path d="M36.7928932,-12.5 L35.3786797,85.4949494" id="Line" transform="translate(36.292893, 36.500000) rotate(-45.000000) translate(-36.292893, -36.500000) "></path>
	                <path d="M36.5,-12.5 L35.0857864,85.4949494" id="Line" transform="translate(36.000000, 36.500000) scale(-1, 1) rotate(-45.000000) translate(-36.000000, -36.500000) "></path>
		            </g>
			        </g>
				    </g>
					</svg>
				</div>
				<div id="article-content"></div>
			</div>
			
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
			<script src='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>
			<script src='/js/vendor.min.js?v2.0.5'></script>
			<script src='/js/main.min.js?v2.0.5'></script>
			<script type="text/javascript">
				var points = [];
				var lastPoint;
				var hasTouch = true;
				
				var parallax = jQuery('div.parallax');
				
				document.addEventListener('touchstart', touchStart, false);
				document.addEventListener('touchmove', touchMove, false);
				document.addEventListener('touchend', touchEnd, false);
				
				function touchStart(e) {
				  parallax.stop();
				  startPoint = null;
				  lastPoint = null;
				  points = [];
				}
				
				function touchMove(e) {
				  if (e) {
				    points.push(e.touches[0].pageY);
				    if (startPoint == null) {
				        startPoint = e.touches[0].pageY;
				    }
				    lastPoint = e.touches[0].pageY;
				  }
				}
				
				function touchEnd(e) {
				  if (points.length >= 2) {
				    var velocity = points[points.length - 1] - points[points.length - 2];
				    
				    var direction = startPoint - lastPoint >= 0 ? 1 : -1;
				    parallax.animate({ scrollTop: parallax.scrollTop() + (-20 * velocity) }, { duration: Math.ceil(Math.sqrt(Math.abs(velocity)) * 50), easing: 'easeOutQuad' });
				  }
				  else {
				  }
				}
			</script>
      
    </body>
</html>