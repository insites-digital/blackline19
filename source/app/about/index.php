<?php $config = require_once '../inc/config.php'; ?>
<?php include  '../inc/header.php'; ?>

<?php // include  'components/component_article.php'; ?>

<main>
	<article id="article-content">
	  <div class="credits component component_text-block">
	    <div class="content">
	        <p>We are an independent venture creative agency. We enjoy making everything work, look, sound and feel better than people imagine, ultimately enabling brands to find their purpose.</p>
	        <p class="text-smaller">Our team has experience in retail, fashion, consumer lifestyle, sports, cycling, real estate, tourism, logistics and food. We are skilled in business, creative direction, brand and creative strategy, art direction, media planning, shoots and campaign creation.</p>
	    </div>
		</div>  
		<div class="contact component component_text-block">
	    <h2 class="brand">Contact</h2>
	    <div class="content">
	      <p><a href="mailto:hello@blacklinecreative.co.uk">hello@blacklinecreative.co.uk</a>
	      <br>
	      <a tel="+442030112450">+44(0)20 3011 2450</a>
	      </p>    
	      <p>The Old Truman Brewery<br>
	          91 Brick Lane<br>
	          London E1 6QL</p>
	      <div class="social-links">
					<a href="https://www.instagram.com/blackline_creative/" target="_blank">Instagram</a>
					<a href="https://www.linkedin.com/company/blackline-creative" target="_blank">LinkedIn</a>
	      </div>
	    </div>
		</div>  
	</article>
</main>


<?php include  '../inc/footer.php'; ?>
