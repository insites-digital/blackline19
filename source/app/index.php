<?php $config = require_once 'inc/config.php'; ?>
<?php include  'inc/header.php'; ?>

<main>
	<div class="parallax">
		<div class="parallax__group section-one">
			<div class="parallax__layer section-title">
        <svg width="1491" height="243" viewBox="0 0 1491 243" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M157.954 242.772C234.815 242.772 255.977 204.342 255.977 169.974C255.977 141.363 239.386 122.74 209.251 115.969C232.106 109.366 247.343 91.7591 247.343 64.8408C247.343 33.0129 231.429 0 155.245 0H0V242.772H157.954ZM56.3759 195.707V140.855H156.6C189.274 140.855 198.416 149.997 198.416 168.281C198.416 186.396 189.274 195.707 156.6 195.707H56.3759ZM56.3759 95.3143V47.2339H156.6C183.01 47.2339 190.459 57.561 190.459 71.2741C190.459 84.9872 183.01 95.3143 156.6 95.3143H56.3759Z" fill="black"/>
          <path d="M349.599 242.772V159.139H439.157L487.745 242.772H550.554L497.734 151.86C529.731 141.194 548.184 118.339 548.184 82.1091C548.184 39.2769 518.557 0 452.701 0H293.223V242.772H349.599ZM452.701 48.9269C481.989 48.9269 491.131 64.5022 491.131 82.1091C491.131 99.716 481.989 115.291 452.701 115.291H349.599V48.9269H452.701Z" fill="black"/>
          <path d="M821.938 242.772H882.038L750.494 0H704.107L572.563 242.772H632.663L658.397 195.2H796.204L821.938 242.772ZM727.301 67.7189L770.641 147.966H683.96L727.301 67.7189Z" fill="black"/>
          <path d="M910.819 242.772H967.195V66.3645L1101.96 242.772H1173.74V0H1117.36V176.408L982.601 0H910.819V242.772Z" fill="black"/>
          <path d="M1491 121.386C1491 53.1593 1451.55 0 1369.78 0H1217.92V242.772H1369.78C1451.55 242.772 1491 189.782 1491 121.386ZM1274.3 48.9269H1369.78C1416.51 48.9269 1432.93 83.8021 1432.93 121.386C1432.93 158.97 1416.51 193.845 1369.78 193.845H1274.3V48.9269Z" fill="black"/>
        </svg>
			</div>
			<div class="parallax__layer parallaxCenterTop">
				<video muted playsinline autoplay loop class="lazy" rel="showVideo">
					<data-src src="https://player.vimeo.com/external/325631622.hd.mp4?s=23a0540dde30aae0bd55fefaabf1c6d2e97908dd&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>  
			<div class="parallax__layer blob blob-first">
				<img class="lazy" data-src="assets/images/BL_GREEN_2-min.png">
			</div>   
			<div class="parallax__layer parallaxLeftTop">
				<img class="lazy" data-src="assets/images/Letters.gif">
			</div>   
			<div class="parallax__layer parallaxLeftDeep">
				<img class="lazy" data-src="assets/images/Men_CMYK-min.jpg">
			</div>   
			<div class="parallax__layer parallaxRight">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/325631950.hd.mp4?s=eaa4e4a2e097c01250d93df5786196241e2a7562&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>   
			<div class="parallax__layer parallaxDeep">
				<img class="lazy" data-src="assets/images/Roof-Pattern-min.jpg">
			</div>   
		</div>

		<div class="parallax__group section-two">
			<div class="parallax__layer section-title">
        <svg width="1901" height="213" viewBox="0 0 1901 213" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M201.766 20.7583C201.766 20.7583 174.579 0.438477 105.071 0.438477C31.9199 0.438477 8.9375 29.5869 8.9375 60.417C8.9375 94.4702 32.2002 112.127 102.969 126.562C146.972 135.53 160.706 137.772 160.706 151.226C160.706 164.398 143.048 172.106 105.071 172.106C55.7432 172.106 23.6519 155.71 23.6519 155.71L0.80957 188.782C0.80957 188.782 34.022 212.605 105.071 212.605C169.814 212.605 207.932 191.725 207.932 151.226C207.932 112.127 176.961 98.394 111.798 85.6416C73.8208 78.2144 56.1636 74.0103 56.1636 60.417C56.1636 48.2251 70.5977 40.938 105.071 40.938C151.036 40.938 178.923 53.8306 178.923 53.8306L201.766 20.7583Z" fill="black"/>
          <path d="M227.551 6.04395V46.5435H313.875V207H360.541V46.5435H446.865V6.04395H227.551Z" fill="black"/>
          <path d="M521.698 207V137.772H595.83L636.049 207H688.04L644.317 131.747C670.803 122.918 686.078 104 686.078 74.0103C686.078 38.5557 661.554 6.04395 607.041 6.04395H475.032V207H521.698ZM607.041 46.5435C631.285 46.5435 638.852 59.436 638.852 74.0103C638.852 88.5845 631.285 101.477 607.041 101.477H521.698V46.5435H607.041Z" fill="black"/>
          <path d="M912.679 207H962.428L853.542 6.04395H815.144L706.258 207H756.006L777.307 167.622H891.378L912.679 207ZM834.343 62.0986L870.218 128.523H798.468L834.343 62.0986Z" fill="black"/>
          <path d="M932.999 6.04395V46.5435H1019.32V207H1065.99V46.5435H1152.31V6.04395H932.999Z" fill="black"/>
          <path d="M1227.15 85.9219V46.5435H1376.11V6.04395H1180.48V207H1377.51V166.5H1227.15V125.02H1370.09V85.9219H1227.15Z" fill="black"/>
          <path d="M1453.61 106.522C1453.61 72.3286 1477.71 40.938 1539.51 40.938C1578.05 40.938 1607.06 57.334 1607.06 57.334L1629.34 24.6821C1629.34 24.6821 1602.43 0.438477 1539.51 0.438477C1448.42 0.438477 1405.54 48.9258 1405.54 106.522C1405.54 164.118 1448.42 212.605 1539.51 212.605C1571.18 212.605 1610.14 208.401 1632.28 188.782V88.4443H1536.15V127.542H1588.98V164.398C1579.87 169.023 1561.65 172.106 1539.51 172.106C1477.71 172.106 1453.61 140.715 1453.61 106.522Z" fill="black"/>
          <path d="M1900.5 6.04395H1846.41L1776.9 89.8457L1707.39 6.04395H1653.3L1753.5 126.281V207H1800.31V126.281L1900.5 6.04395Z" fill="black"/>
        </svg>
			</div>
			<div class="parallax__layer parallaxCenterTop">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/325632016.hd.mp4?s=9d89b58a1d10b7c6c2614cf4c4b22702534ee23c&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>  
			<div class="parallax__layer parallaxLeftMid">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/325632059.hd.mp4?s=a042e2885817dc72384432dbf28dcc03cab5fe1c&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>   
			<div class="parallax__layer parallaxRight">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/328458329.hd.mp4?s=084db9d3ef649413d5dff872fe8cbd1d3ebbfce4&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>   
			<div class="parallax__layer parallaxDeep">
				<video muted playsinline autoplay loop class="lazy" rel="showVideo">>
					<data-src src="https://player.vimeo.com/external/325632120.hd.mp4?s=47aa0a2113a40cfdb05ae54ad1339cf83d556d19&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>   
		</div>

		<div class="parallax__group section-three">
			<div class="parallax__layer parallaxLeftMid">
				<img class="lazy" data-src="assets/images/Tech-image-min.jpg">
			</div>   
			<div class="parallax__layer parallaxLeftTop first">
				<img class="lazy" data-src="assets/images/PopArtPoster-min.jpg">
			</div>   
			<div class="parallax__layer parallaxCenterTop first">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/325632483.hd.mp4?s=8855e651f288062beafef0f3e4c8011e41c19376&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>  
			<div class="parallax__layer parallaxRight first">
				<img class="lazy" data-src="assets/images/Shoes_2-min.jpg">
			</div>   
			<div class="parallax__layer parallaxDeep">
				<img class="lazy" data-src="assets/images/Crown-min.jpg">
			</div>   
			<div class="parallax__layer parallaxCenterTop second">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/325632543.hd.mp4?s=5abde5d29b3848fe1b1eb46ccacac88471abcb8c&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>  
			<div class="parallax__layer parallaxLeftTop second">
				<img class="lazy" data-src="assets/images/Construction-icons.gif">
			</div>   
			<div class="parallax__layer parallaxRight second">
				<img class="lazy" data-src="assets/images/BYO-min.jpg">
			</div>   
			<div class="parallax__layer parallaxDeep second">
				<img class="lazy" data-src="assets/images/Beats-min.jpg">
			</div>   
		</div>

		<div class="parallax__group section-four">
			<div class="parallax__layer section-title">
        <svg width="1858" height="213" viewBox="0 0 1858 213" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M48.6792 106.522C48.6792 72.3286 72.7827 40.938 134.583 40.938C173.121 40.938 202.129 57.334 202.129 57.334L224.411 24.6821C224.411 24.6821 197.504 0.438477 134.583 0.438477C43.4941 0.438477 0.612305 48.9258 0.612305 106.522C0.612305 164.118 43.4941 212.605 134.583 212.605C197.504 212.605 224.411 188.362 224.411 188.362L202.129 155.71C202.129 155.71 173.121 172.106 134.583 172.106C72.7827 172.106 48.6792 140.715 48.6792 106.522Z" fill="black"/>
          <path d="M446.387 207H496.136L387.25 6.04395H348.852L239.966 207H289.714L311.015 167.622H425.086L446.387 207ZM368.051 62.0986L403.926 128.523H332.176L368.051 62.0986Z" fill="black"/>
          <path d="M579.797 6.04395H519.959V207H566.625V78.0742L630.947 207H667.103L731.425 78.0742V207H778.091V6.04395H718.252L649.025 144.779L579.797 6.04395Z" fill="black"/>
          <path d="M945.134 46.5435C967.696 46.5435 978.626 57.6143 978.626 76.3926C978.626 95.1709 967.696 106.242 945.134 106.242H861.192V46.5435H945.134ZM861.192 207V146.741H945.134C994.742 146.741 1025.85 121.236 1025.85 76.3926C1025.85 31.5488 994.742 6.04395 945.134 6.04395H814.526V207H861.192Z" fill="black"/>
          <path d="M1224.99 207H1274.74L1165.85 6.04395H1127.45L1018.57 207H1068.31L1089.61 167.622H1203.69L1224.99 207ZM1146.65 62.0986L1182.53 128.523H1110.78L1146.65 62.0986Z" fill="black"/>
          <path d="M1299.96 207H1346.63V6.04395H1299.96V207Z" fill="black"/>
          <path d="M1427.06 106.522C1427.06 72.3286 1451.17 40.938 1512.97 40.938C1551.51 40.938 1580.51 57.334 1580.51 57.334L1602.8 24.6821C1602.8 24.6821 1575.89 0.438477 1512.97 0.438477C1421.88 0.438477 1379 48.9258 1379 106.522C1379 164.118 1421.88 212.605 1512.97 212.605C1544.64 212.605 1583.6 208.401 1605.74 188.782V88.4443H1509.6V127.542H1562.44V164.398C1553.33 169.023 1535.11 172.106 1512.97 172.106C1451.17 172.106 1427.06 140.715 1427.06 106.522Z" fill="black"/>
          <path d="M1639.37 207H1686.04V60.9775L1797.59 207H1857V6.04395H1810.34V152.066L1698.79 6.04395H1639.37V207Z" fill="black"/>
        </svg>
			</div>
			<div class="parallax__layer blob blob-first">
				<img class="lazy" data-src="assets/images/BL_GREEN_2-min.png">
			</div>   
			<div class="parallax__layer parallaxLeftTop first">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/325632619.hd.mp4?s=91da3ac87dfe50d8db64fcc8267f1d71849c2b3e&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>   
			<div class="parallax__layer parallaxCenterTop first">
				<img class="lazy" data-src="assets/images/Icons.gif">
			</div>  
			<div class="parallax__layer parallaxDeep">
				<img class="lazy" data-src="assets/images/Home-min.jpg">
			</div>   
			<div class="parallax__layer parallaxLeftTop second">
				<img class="lazy" data-src="assets/images/kingston.svg">
			</div>   
			<div class="parallax__layer parallaxDeep second">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/325632712.hd.mp4?s=a5409803f7b3f3d242ea058fd739258958bfa051&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>   
		</div>
		
		<div class="parallax__group section-five">
			<div class="parallax__layer section-title">
        <svg width="2746" height="213" viewBox="0 0 2746 213" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M130.607 46.105C153.169 46.105 164.1 57.1758 164.1 75.9541C164.1 94.7324 153.169 105.803 130.607 105.803H46.6655V46.105H130.607ZM46.6655 206.562V146.303H130.607C180.216 146.303 211.326 120.798 211.326 75.9541C211.326 31.1104 180.216 5.60547 130.607 5.60547H0V206.562H46.6655Z" fill="black"/>
        <path d="M291.625 85.4834V46.105H440.59V5.60547H244.959V206.562H441.991V166.062H291.625V124.582H434.564V85.4834H291.625Z" fill="black"/>
        <path d="M522.29 206.562V137.334H596.422L636.641 206.562H688.632L644.909 131.308C671.395 122.479 686.67 103.561 686.67 73.5718C686.67 38.1172 662.146 5.60547 607.633 5.60547H475.624V206.562H522.29ZM607.633 46.105C631.876 46.105 639.444 58.9976 639.444 73.5718C639.444 88.146 631.876 101.039 607.633 101.039H522.29V46.105H607.633Z" fill="black"/>
        <path d="M766.127 88.2861V46.105H915.093V5.60547H719.462V206.562H766.127V127.384H909.067V88.2861H766.127Z" fill="black"/>
        <path d="M1077.09 212.167C1168.18 212.167 1211.06 163.68 1211.06 106.083C1211.06 48.4873 1168.18 0 1077.09 0C986.002 0 943.12 48.4873 943.12 106.083C943.12 163.68 986.002 212.167 1077.09 212.167ZM1077.09 171.667C1015.29 171.667 991.187 140.277 991.187 106.083C991.187 71.8901 1015.29 40.4995 1077.09 40.4995C1138.89 40.4995 1162.99 71.8901 1162.99 106.083C1162.99 140.277 1138.89 171.667 1077.09 171.667Z" fill="black"/>
        <path d="M1288.56 206.562V137.334H1362.69L1402.91 206.562H1454.9L1411.18 131.308C1437.66 122.479 1452.94 103.561 1452.94 73.5718C1452.94 38.1172 1428.41 5.60547 1373.9 5.60547H1241.89V206.562H1288.56ZM1373.9 46.105C1398.14 46.105 1405.71 58.9976 1405.71 73.5718C1405.71 88.146 1398.14 101.039 1373.9 101.039H1288.56V46.105H1373.9Z" fill="black"/>
        <path d="M1545.57 5.60547H1485.73V206.562H1532.4V77.6357L1596.72 206.562H1632.87L1697.2 77.6357V206.562H1743.86V5.60547H1684.02L1614.8 144.341L1545.57 5.60547Z" fill="black"/>
        <path d="M1974.11 206.562H2023.85L1914.97 5.60547H1876.57L1767.68 206.562H1817.43L1838.73 167.183H1952.81L1974.11 206.562ZM1895.77 61.6602L1931.64 128.085H1859.89L1895.77 61.6602Z" fill="black"/>
        <path d="M2047.68 206.562H2094.34V60.5391L2205.89 206.562H2265.31V5.60547H2218.64V151.628L2107.1 5.60547H2047.68V206.562Z" fill="black"/>
        <path d="M2344.35 106.083C2344.35 71.8901 2368.45 40.4995 2430.25 40.4995C2468.79 40.4995 2497.8 56.8955 2497.8 56.8955L2520.08 24.2437C2520.08 24.2437 2493.17 0 2430.25 0C2339.16 0 2296.28 48.4873 2296.28 106.083C2296.28 163.68 2339.16 212.167 2430.25 212.167C2493.17 212.167 2520.08 187.923 2520.08 187.923L2497.8 155.271C2497.8 155.271 2468.79 171.667 2430.25 171.667C2368.45 171.667 2344.35 140.277 2344.35 106.083Z" fill="black"/>
        <path d="M2594.91 85.4834V46.105H2743.88V5.60547H2548.25V206.562H2745.28V166.062H2594.91V124.582H2737.85V85.4834H2594.91Z" fill="black"/>
        </svg>
			</div>
			<div class="parallax__layer parallaxLeftTop first">
				<img class="lazy" data-src="assets/images/All-Animals_onred.gif">
			</div>   
			<div class="parallax__layer parallaxDeep">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/325632855.hd.mp4?s=cfea806b4c0165107c26ef2f50fafff0412b0077&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>   
			<div class="parallax__layer parallaxRight first">
				<img class="lazy" data-src="assets/images/111.svg">
			</div>
			<div class="parallax__layer parallaxDeep second">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/326769389.hd.mp4?s=c720d502a1a004b93f36cb36239fa3fc0dd5134d&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>   
			<div class="parallax__layer parallaxRight second">
				<img class="lazy" data-src="assets/images/33-min.jpg">
			</div>
			<div class="parallax__layer parallaxCenterTop">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/325633001.hd.mp4?s=389aee6edd25ebf183eeef4b5604a9121ace42ca&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>
			<div class="parallax__layer parallaxLeftMid">
				<img class="lazy" data-src="assets/images/Knifes-and-Forks-min.jpg">
			</div>
			<div class="parallax__layer parallaxRight third">
				<img class="lazy" data-src="assets/images/Poster-Design-min.jpg">
			</div>
			<div class="parallax__layer parallaxDeep third">
				<video muted playsinline autoplay loop class="lazy">
					<data-src src="https://player.vimeo.com/external/325633067.hd.mp4?s=bad89db0ae7f542fada627c564ff26cfbab28949&profile_id=174" type="video/mp4"></data-src>
				</video>
			</div>   
		</div>


		<div class="credits component component_text-block first">
			<div class="text-translate">
				<h2>BRAND</h2>
				<ul>
					<li>STRATEGY &amp; IDENTITY</li>
          <li>CAMPAIGN CREATIVE</li>
          <li>CONTENT CREATION</li>
          <li>ART DIRECTION</li>
          <li>PRODUCTION</li>
				</ul>
				<h2>MARKETING</h2>
				<ul>
					<li>STRATEGY &amp; PLAN</li>
          <li>PERFORMANCE</li>
          <li>MEDIA PLANNING</li>
          <li>SOCIAL &amp; DIGITAL</li>
				</ul>
			</div>
		</div>	
		<div class="contact component component_text-block" style="padding-bottom: 300px;">
			<div class="text-translate">
				<h2>Contact</h2>
				<div class="content">
					<p><a href="mailto:hello@blacklinecreative.co.uk">hello@blacklinecreative.co.uk</a>
					<br>
					<a tel="+442030112450">+44(0)20 3011 2450</a>
					</p>	
					<p>The Old Truman Brewery<br>
						91 Brick Lane<br>
						London E1 6QL</p>
					<div class="social-links">
						<a href="https://www.instagram.com/blackline_creative/" target="_blank">Instagram</a>
						<a href="https://www.linkedin.com/company/blackline-creative" target="_blank">LinkedIn</a>
					</div>
				</div>
			</div>
		</div>	
		
	</div>
</main>


<?php include  'inc/footer.php'; ?>
